# dp_root

 - root folder/repository of the HDR image capture and reconstruction project
 - contains other projects as submodules (clone recursively)

	git clone --recursive git@bitbucket.org:hdri/dp_root.git
